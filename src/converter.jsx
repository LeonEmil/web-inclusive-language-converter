import React from 'react'
import transform from './functions'

class Converter extends React.Component {
    constructor(props){
        super(props)

        this.state = {
            text: "",
        }

        this.text = this.text.bind(this)
    }

    text = (e) => {
        this.setState({
            text: transform(e.target.value)
        })
    }

    

    render(){
        return(
        <>
            <div className="App">
                <div className="text">
                    <h1>Combertidor de lenguaje inclusivo</h1>
                    <p>Escriba un texto y se combertirá automáticamente a lenguaje inclusivo no sexista</p>
                </div>
                <textarea name="textarea" className="textarea" onChange={this.text}>
                </textarea>

                <div className="text-converted">
                        <p>{this.state.text}</p>
                </div>
            </div>
        </>
        )
    }
}

export default Converter