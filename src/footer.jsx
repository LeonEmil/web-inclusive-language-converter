import React from 'react'

const Footer = () => {
    return(
        <footer className="footer">
            <div>
                <span>Desarrollado por León Emil</span>
            </div>
            <div>
                <div>
                    <a href="https://gitlab.com/LeonEmil/" className="link">GitLab</a>
                </div>
                <div>
                    <a href="https://codepen.io/LeonEmil/" className="link">Codepen</a>
                </div>
            </div>
        </footer>
    )
}

export default Footer