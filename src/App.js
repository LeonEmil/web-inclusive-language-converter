import React from 'react';

import './App.css';
import Converter from './converter'
import Footer from './footer';

function App() {
  return (
    <>
      <Converter />
      <Footer />
    </>
  );
}

export default App;
