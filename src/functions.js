// import exceptions from './exceptions'

const transform = (text) => {
    let words = text.split(" ")
    let newWords = []
    
    words.forEach(word => {
        
        if (word.endsWith("a") && word.toLowerCase() !== "hola" && word.toLowerCase() !== "mama" && word.toLowerCase() !== "mamas" && word.toLowerCase() !== "papa" && word.toLowerCase() !== "papas" && word.toLowerCase() !== "casa" && word.toLowerCase() !== "caza" && word.toLowerCase() !== "a" && word.toLowerCase() !== "lava" && word.toLowerCase() !== "para" && word.toLowerCase() !== "dia" && word.toLowerCase() !== "dias" && word.toLowerCase() !== "día" && word.toLowerCase() !== "días" && word.toLowerCase() !== "contra" && word.toLowerCase() !== "hacia" && word.toLowerCase() !== "hasta"){
            let newWord = word.substring(0, word.length - 1) + word.substr(-1, 1).replace("a", "e")
            newWords.push(newWord) 
        } else if (word.endsWith("a,") && word.toLowerCase() !== "hola," && word.toLowerCase() !== "mama," && word.toLowerCase() !== "papa," && word.toLowerCase() !== "casa," && word.toLowerCase() !== "caza," && word.toLowerCase() !== "dia," && word.toLowerCase() !== "día,") {
            let newWord = word.substring(0, word.length - 2) + word.substr(-2, 2).replace("a,", "e,")
            newWords.push(newWord)
        } else if (word.endsWith("a.") && word.toLowerCase() !== "hola." && word.toLowerCase() !== "mama." && word.toLowerCase() !== "papa." && word.toLowerCase() !== "casa." && word.toLowerCase() !== "caza." && word.toLowerCase() !== "dia." && word.toLowerCase() !== "día.") {
            let newWord = word.substring(0, word.length - 2) + word.substr(-2, 2).replace("a.", "e.")
            newWords.push(newWord)
        } else if (word.endsWith("a!") && word.toLowerCase() !== "hola!" && word.toLowerCase() !== "mama!" && word.toLowerCase() !== "papa!" && word.toLowerCase() !== "casa!" && word.toLowerCase() !== "caza!" && word.toLowerCase() !== "dia!" && word.toLowerCase() !== "día!") {
            let newWord = word.substring(0, word.length - 2) + word.substr(-2, 2).replace("a!", "e!")
            newWords.push(newWord)
        } else if (word.endsWith("a?") && word.toLowerCase() !== "hola?" && word.toLowerCase() !== "mama?" && word.toLowerCase() !== "papa?" && word.toLowerCase() !== "casa?" && word.toLowerCase() !== "caza?" && word.toLowerCase() !== "dia?" && word.toLowerCase() !== "día?") {
            let newWord = word.substring(0, word.length - 2) + word.substr(-2, 2).replace("a?", "e?")
            newWords.push(newWord)
        } else if (word.endsWith("a)") && word.toLowerCase() !== "hola)" && word.toLowerCase() !== "mama)" && word.toLowerCase() !== "papa)" && word.toLowerCase() !== "casa)" && word.toLowerCase() !== "caza)" && word.toLowerCase() !== "dia)" && word.toLowerCase() !== "día)") {
            let newWord = word.substring(0, word.length - 2) + word.substr(-2, 2).replace("a)", "e)")
            newWords.push(newWord)
        } else if (word.endsWith('a"') && word.toLowerCase() !== 'hola"' && word.toLowerCase() !== 'mama"' && word.toLowerCase() !== 'papa"' && word.toLowerCase() !== 'casa"' && word.toLowerCase() !== 'caza"' && word.toLowerCase() !== 'dia"' && word.toLowerCase() !== 'día"') {
            let newWord = word.substring(0, word.length - 2) + word.substr(-2, 2).replace('a"', 'e"')
            newWords.push(newWord) 

        } else if (word.endsWith("o") && word.toLowerCase() !== "como" && word.toLowerCase() !== "inclusivo" && word.toLowerCase() !== "inclusivos" && word.toLowerCase() !== "espero"){
            let newWord = word.substring(0, word.length - 1) + word.substr(-1, 1).replace("o", "e")
            newWords.push(newWord)
        } else if (word.endsWith("o,") && word.toLowerCase() !== "como," && word.toLowerCase() !== "inclusivo," && word.toLowerCase() !== "espero,"){
            let newWord = word.substring(0, word.length - 2) + word.substr(-2, 2).replace("o,", "e,")
            newWords.push(newWord)
        } else if (word.endsWith("o.") && word.toLowerCase() !== "como." && word.toLowerCase() !== "inclusivo." && word.toLowerCase() !== "espero."){
            let newWord = word.substring(0, word.length - 2) + word.substr(-2, 2).replace("o.", "e.")
            newWords.push(newWord)
        } else if (word.endsWith("o!") && word.toLowerCase() !== "como!" && word.toLowerCase() !== "inclusivo!" && word.toLowerCase() !== "espero!"){
            let newWord = word.substring(0, word.length - 2) + word.substr(-2, 2).replace("o!", "e!")
            newWords.push(newWord)
        } else if (word.endsWith("o?") && word.toLowerCase() !== "como?" && word.toLowerCase() !== "inclusivo?" && word.toLowerCase() !== "espero?"){
            let newWord = word.substring(0, word.length - 2) + word.substr(-2, 2).replace("o?", "e?")
            newWords.push(newWord)
        } else if (word.endsWith("o)") && word.toLowerCase() !== "como)" && word.toLowerCase() !== "inclusivo)" && word.toLowerCase() !== "espero)"){
            let newWord = word.substring(0, word.length - 2) + word.substr(-2, 2).replace("o)", "e)")
            newWords.push(newWord)
        } else if (word.endsWith('o"') && word.toLowerCase() !== 'como"' && word.toLowerCase() !== 'inclusivo"' && word.toLowerCase() !== 'espero"'){
            let newWord = word.substring(0, word.length - 2) + word.substr(-2, 2).replace('o"', 'e"')
            newWords.push(newWord)

        } else if (word.endsWith("as") && word.toLowerCase() !== "olas" && word.toLowerCase() !== "gracias" && word.toLowerCase() !== "tras"){
            let newWord = word.substring(0, word.length - 2) + word.substr(-2, 2).replace("as", "es")
            newWords.push(newWord)
        } else if (word.endsWith("as,") && word.toLowerCase() !== "olas," && word.toLowerCase() !== "gracias,") {
            let newWord = word.substring(0, word.length - 3) + word.substr(-3, 3).replace("as,", "es,")
            newWords.push(newWord)
        } else if (word.endsWith("as.") && word.toLowerCase() !== "olas." && word.toLowerCase() !== "gracias.") {
            let newWord = word.substring(0, word.length - 3) + word.substr(-3, 3).replace("as.", "es.")
            newWords.push(newWord)
        } else if (word.endsWith("as!") && word.toLowerCase() !== "olas!" && word.toLowerCase() !== "gracias!") {
            let newWord = word.substring(0, word.length - 3) + word.substr(-3, 3).replace("as!", "es!")
            newWords.push(newWord)
        } else if (word.endsWith("as?") && word.toLowerCase() !== "olas?" && word.toLowerCase() !== "gracias?") {
            let newWord = word.substring(0, word.length - 3) + word.substr(-3, 3).replace("as?", "es?")
            newWords.push(newWord)
        } else if (word.endsWith("as)") && word.toLowerCase() !== "olas)" && word.toLowerCase() !== "gracias)") {
            let newWord = word.substring(0, word.length - 3) + word.substr(-3, 3).replace("as)", "es)")
            newWords.push(newWord)
        } else if (word.endsWith('as"') && word.toLowerCase() !== 'olas"' && word.toLowerCase() !== 'gracias"') {
            let newWord = word.substring(0, word.length - 3) + word.substr(-3, 3).replace('as"', 'es"')
            newWords.push(newWord)

        } else if (word.endsWith("os")){
            let newWord = word.substring(0, word.length - 2) + word.substr(-2, 2).replace("os", "es")
            newWords.push(newWord)
        } else if (word.endsWith("os,")) {
            let newWord = word.substring(0, word.length - 3) + word.substr(-3, 3).replace("os,", "es,")
            newWords.push(newWord)
        } else if (word.endsWith("os.")) {
            let newWord = word.substring(0, word.length - 3) + word.substr(-3, 3).replace("os.", "es.")
            newWords.push(newWord)
        } else if (word.endsWith("os!")) {
            let newWord = word.substring(0, word.length - 3) + word.substr(-3, 3).replace("os!", "es!")
            newWords.push(newWord)
        } else if (word.endsWith("os?")) {
            let newWord = word.substring(0, word.length - 3) + word.substr(-3, 3).replace("os?", "es?")
            newWords.push(newWord)
        } else if (word.endsWith("os)")) {
            let newWord = word.substring(0, word.length - 3) + word.substr(-3, 3).replace("os)", "es)")
            newWords.push(newWord)
        } else if (word.endsWith('os"')) {
            let newWord = word.substring(0, word.length - 3) + word.substr(-3, 3).replace('os"', 'es"')
            newWords.push(newWord)      
         
        } else {
            newWords.push(word)
        }
    });
    
    newWords = newWords.join(" ")
    //console.log(newWords)

    return newWords
}

export default transform
